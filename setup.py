#!/usr/bin/env python

from distutils.core import setup

setup(
    name='my-semver',
    use_scm_version = True,
    setup_requires=[
        'setuptools_scm',
    ],
    description='My Semantic Versioning',
    packages=['semver'],
    )